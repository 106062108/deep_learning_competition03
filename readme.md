## Project GitLab Link: 
- https://gitlab.com/106062108/deep_learning_competition03?fbclid=IwAR2pWZWp8m1Icu77Z1LTd1g6yrXxCCiGiH_xqIkNEyxsXvYbw-cJkrhTZ1E

## Enviroment Setting

- install pip package 'transformers' <br>
	You should install 'transformers' by the following command: <br>
	pip install transformers <br>

- file
	You should put these files under this architecture: <br>
	
	```python
	'''
	dataset
		|- testDataBERT.pkl
		|- text2ImgDataBERT.pkl
		|- ...
	...
	Text_Embedding
	'''
	```
- other file
	The following files are the plk files of different embedding methods.
	```python
	'''
	dataset
		others
			|- testDataBERT-embedding-768.pkl           # testing dataset using BERT.text2embedding with shape 768
			|- text2ImgDataBERT-embedding-768.pkl       # training dataset using BERT.text2embedding with shape 768
			|- testDataBERT-model_embedding-20.pkl      # testing dataset using BERT.text2modelembedding with shape 20
			|- text2ImgDataBERT-model_embedding-20.pkl  # training dataset using BERT.text2modelembedding with shape 20
	'''
	```

- ***Note***
	
	The text encoder use BERT to transform text into bert-based embedding.
	
	I've transformed training and testing dataset into bert-based embedding (with shape=BERT.max_seq_length) (see class BERT), 
	and store them as plk files named 'text2ImgDataBERT.plk' and 'testDataBERT.plk' respectively.
	
	You can load the above plk files as training and testing dataset, and use them on training.
	
	If you want different shape of embedding, you can change BERT.max_seq_length as you want,
	and call BERT.transform_training_dataset and BERT.transform_testing_dataset to transform training and testing dataset respectively.
	
	If you want to get an individual embedding, you can call BERT.text2embedding with an input text to get a embedding with shape=BERT.max_seq_length.
	

## API Introduction

```python
def IdList2sent(idList):
	'''
	inputs  - idList
	outputs - text
	convert idList to text, and change some tokens into bert-based tokens.
	input an idList, and return text.
	'''
```

```python
class BERT:
	
	def __init__():
		'''
		parameters - (transformers.AutoTokenizer) bert_tokenizer
		           - (transformers.TFAutoModel) bert_model
		
		max_seq_length: define the shape of text embedding (default=768) (see text2embedding, text2modelembedding)
		max_encode_length: define the shape of text encode using bert_tokenizer (default=50) (see text2encode)
		'''
		
	def create_model():
		'''
		create an BERT-based model that input a bert-formatted encode, and return embedding as output.
		'''

	def text2encode():
		'''
		inputs  - text
		outputs - encode (shape=max_encode_length)
		encode text using bert tokenizer.
		input a text, and return bert-formatted encode as output.
		'''
		
	def text2embedding():
		'''
		inputs  - text
		outputs - embedding (shape=max_seq_length)
		embed text using bert tokenizer and original bert model from package 'transformers'.
		input a text, and return embedding as output.
		'''

	
	def text2modelembedding():
		'''
		inputs  - text
		outputs - embedding (shape=max_seq_length)
		embed text using bert tokenizer and bert-based model created by function create_model().
		input a text, and return embedding as output.
		'''
	
	def transform_training_dataset():
		'''
		inputs  - dataset
		        - plk_save_path=None
		outputs - dataset
		transform 'Captions' of training dataset as original bert model text embedding, and store it as plk file, 
		input a dataset, and return a dataset whose 'Captions' be changed as original bert text embedding.
		'''
		
	def transform_testing_dataset():
		'''
		inputs  - dataset
		        - plk_save_path=None
		outputs - dataset
		transform 'Captions' of testing dataset as original bert model text embedding, and store it as plk file, 
		input a dataset, and return a dataset whose 'Captions' be changed as original bert text embedding.
		'''
```